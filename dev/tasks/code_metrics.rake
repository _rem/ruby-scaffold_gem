# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

desc "lib potentially uncalled / dead methods"
task :debride do
  sh "debride lib"
end

desc "lib structural similarities"
task :flay do
  sh "flay -d lib"
end

namespace :flog do
  desc "lib ABC metric"
  task :metric do
    sh "flog -aged lib"
  end

  desc "lib ABC score"
  task :score do
    sh "flog -s lib"
  end
end
