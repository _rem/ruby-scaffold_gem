# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require_relative "./prep_release"

desc "Release built gem on Rubygems"
task :release => [:tag] do
  PrepRelease.publish_gem
end

desc "Tag release commit"
task :tag => [:commit_checksum] do
  checksum_dir = File.expand_path "../../../checksum", __FILE__
  PrepRelease.tag_release_commit archive: checksum_dir
end

desc "Commit latest packed gem checksum"
task :commit_checksum => [:checksum] do
  checksum_dir = File.expand_path "../../../checksum", __FILE__
  PrepRelease.commit_checksum archive: checksum_dir
end

desc "Create checksum file for latest packed gem"
task :checksum => [:pack] do
  checksum_dir = File.expand_path "../../../checksum", __FILE__
  gem = Dir["lorca-*.gem"].first

  PrepRelease.checksum gem, archive: checksum_dir
  puts "Checksum created successfully"
end

desc "Pack gem"
task :pack => [:test] do
  PrepRelease.package
end
